/**
 * Transform a TypeAppareil into a string
 * @author Jimmy Tournemaine
 * @author Pierre Siguret
 */
public class TypeAppareilToStringTransformer {
	
	/**
	 * Transform the enum item to the associated String value
	 * @param ta The item in the enum
	 * @return The corresponding string
	 */
	public static String stringValue(TypeAppareil ta) {
		switch(ta) {
		case ALARME:
			return "ALARME";
		case CHAUFFAGE:
			return "CHAUFFAGE";
		case ECLAIRAGE:
			return "ECLAIRAGE";
		case FENETRE:
			return "FENETRE";
		case OTHER:
			return "OTHER";
		case OTHER_CAFETIERE:
			return "AUTRE_APPAREIL_CAFE";
		case OTHER_LAV_LIN:
			return "AUTRE_APPAREIL_LL";
		case OTHER_LAV_VAI:
			return "AUTRE_APPAREIL_LV";
		case OTHER_ORDINATEUR:
			return "AUTRE_APPAREIL_ORDINATEUR";
		case OTHER_PORTAIL:
			return "AUTRE_APPAREIL_PORTAIL";
		case OTHER_SEC_LIN:
			return "AUTRE_APPAREIL_SL";
		case OTHER_TV:
			return "AUTRE_APPAREIL_TV";
		case OTHER_VID_PRO:
			return "AUTRE_APPAREIL_VP";
		case OTHER_HIFI:
			return "AUTRE_APPAREIL_HIFI";
		case VOLET:
			return "VOLET";
		}
		throw new RuntimeException("This code should not be reached.");
	}
}
