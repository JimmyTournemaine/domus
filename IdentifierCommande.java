/**
 * An identifier for a command
 * @author Jimmy Tournemaine
 * @author Pierre Siguret
 */
public class IdentifierCommande extends Identifier {

	public IdentifierCommande(String id) {
		super(id);
	}

}
