import java.io.File;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * This class is used by the parser to generate a java file that can be used by a simulator
 * 
 * @author Jimmy Tournemaine
 * @author Pierre Siguret
 */
public class CodeGenerator {

	private FileWriter out;
	private FileWriter outH;
	private String code = "";
	private String associations = "";
	private String scenarios = "";
	private String scenario = "";
	private String currentScenario;
	private String habitat = "";
	private boolean inFor = false;
	private boolean inForIf = false;
	private Identifiers identifiers;
	private int nbProg = 0;
	private String habitats = "";

	/**
	 * Get identifiers informations and create the file CMaisonUser.java if it is needed.
	 * @param identifiers The identifiers
	 */
	public CodeGenerator(Identifiers identifiers) {
		try {
			this.identifiers = identifiers;
			File f = new File("CMaisonUser.java");
			if (!f.exists()) {
				if (!f.createNewFile())
					throw new Exception("Unable to create the file CMaisonUser.java");
			}
			out = new FileWriter(f);
			f = new File("HabitatSpecific.java");
			if (!f.exists()) {
				if (!f.createNewFile())
					throw new Exception("Unable to create the file HabitatSpecific.java");
			}
			outH = new FileWriter(f);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Write data in the file
	 * @throws Exception
	 */
	public void flush() throws Exception {
		writeDeclarations();
		writeSets();
		writeInterfaces();
		code += "\n/* Déclarations des scénarios */\n" + scenarios;
		code += "\n/* Déclarations de commandes */\n" + associations;
		out.write("public class CMaisonUser extends CMaison { public CMaisonUser() { \n\tsuper(); " + code + "monHabitat = new HabitatSpecific(ma_liste_appareils, ma_liste_ens_appareils, ma_liste_scenarios, ma_liste_interfaces, ma_liste_programmations); }}");
		out.flush();
		out.close();
		this.flushH();
	}
	
	/**
	 * Write data in the second file
	 * @throws Exception
	 */
	private void flushH() throws Exception {
		outH.write("import java.util.ArrayList; public class HabitatSpecific extends Habitat {"
				+ "public HabitatSpecific(ArrayList<CAppareil> lapp, ArrayList<CEnsAppareil> lens, ArrayList<CScenario> lscen, ArrayList<CInterface> lint, ArrayList<CProgrammation> lprog){ super(lapp, lens, lscen, lint, lprog); }"
				+ "public void execScenarioNum(int num) { System.err.println(\"Running scenario \"+ this.l_scenarios.get(num).getNomScenario()+\"...\"); switch(num) {"
				+ habitats + "}}}");
		outH.flush();
		outH.close();
	}

	/**
	 * Write the devices declarations
	 * 
	 * @throws Exception
	 */
	private void writeDeclarations() throws Exception {
		code += "\n/* Devices declarations */\n";
		Iterator<IdentifierAppareil> it = identifiers.getAppareils().iterator();
		while (it.hasNext()) {
			IdentifierAppareil ia = it.next();
			switch (ia.type) {
			case ALARME:
				code += String.format("CAlarme %s = new CAlarme(\"%s\",TypeAppareil.ALARME);", ia.id, ia.id);
				break;
			case CHAUFFAGE:
				code += String.format("CChauffage %s = new CChauffage(\"%s\",TypeAppareil.CHAUFFAGE);", ia.id, ia.id);
				break;
			case ECLAIRAGE:
				code += String.format("CEclairage %s = new CEclairage(\"%s\",TypeAppareil.ECLAIRAGE);", ia.id, ia.id);
				break;
			case FENETRE:
				code += String.format("CVoletFenetre %s = new CVoletFenetre(\"%s\",TypeAppareil.FENETRE);", ia.id,
						ia.id);
				break;
			case OTHER:
				code += String.format("CAutreAppareil %s = new COther(\"%s\",TypeAppareil.OTHER);", ia.id, ia.id);
				break;
			case VOLET:
				code += String.format("CVoletFenetre %s = new CVoletFenetre(\"%s\",TypeAppareil.VOLET);", ia.id, ia.id);
				break;
			default:
				code += String.format("CAutreAppareil %s = new CAutreAppareil(\"%s\",TypeAppareil.%s);", ia.id,
						ia.id, TypeAppareilToStringTransformer.stringValue(ia.type));
			}
			code += String.format("ma_liste_appareils.add(%s);", ia.id);
		}
	}

	/**
	 * Write sets declarations
	 */
	private void writeSets() {
		code += "\n/* Déclarations des ensembles */\n";
		Iterator<IdentifierSet> it = identifiers.getSets().iterator();
		while (it.hasNext()) {
			IdentifierSet is = it.next();
			code += String.format("CEnsAppareil %s = new CEnsAppareil(\"%s\");", is.id, is.id);
			for (Identifier i : is.elements) {
				code += String.format("%s.addAppareil(%s);", is.id, i.id);
			}
			code += String.format("ma_liste_ens_appareils.add(%s);", is.id);
		}
	}

	/**
	 * Write interfaces
	 */
	private void writeInterfaces() {
		code += "\n/* Déclarations des interfaces */\n";
		Iterator<IdentifierInterface> it = identifiers.getInterfaces().iterator();
		while (it.hasNext()) {
			IdentifierInterface ii = it.next();
			code += String.format("CInterface %s = new CInterface(\"%s\",TypeInterface.", ii.id, ii.id);
			switch (ii.type) {
			case INTERRUPTEUR:
				code += "INTERRUPTEUR";
				break;
			case MOBILE:
				code += "MOBILE";
				break;
			case TABLETTE:
				code += "TABLETTE";
				break;
			case TELECOMMANDE:
				code += "TELECOMMANDE";
				break;
			case TELEPHONE:
				code += "TELEPHONE";
				break;
			default:
				break;
			}
			code += String.format("); ma_liste_interfaces.add(%s);", ii.id);
		}
	}

	/**
	 * Write a command (association)
	 * @param id The id of the interface
	 * @param scenariiId The identifiers of the scenarii to associate with the interface
	 */
	public void writeCommand(String id, List<String> scenariiId) {
		for (String s : scenariiId) {
			associations += String.format("%s.addScenarioAssocie(\"%s\");", id, s);
		}
	}

	/**
	 * Write a scheduled command
	 * @param idScenar The scenario scheduled
	 * @param dates The dates when you execute the scenario
	 */
	public void writeProgs(String idScenar, ArrayList<LinkedList<Integer>> dates) {
		associations += String.format("CProgrammation p%d = new CProgrammation(\"%s\");", ++nbProg, idScenar);
		for (List<Integer> date : dates) {
			associations += String.format("p%d.addDate(new CDate(%s,%s,%s,%s,%s));", nbProg, date.get(0), date.get(1),
					date.get(2), date.get(3), date.get(4));
		}
	}

	/**
	 * Write a scheduled command
	 * @param idScenar The scenario scheduled
	 * @param dates The date when you execute the scenario
	 */
	public void writeProg(String idScenar, List<Integer> date) {
		associations += String.format("CProgrammation p%d = new CProgrammation(\"%s\");", ++nbProg, idScenar);
		associations += String.format("p%d.addDate(new CDate(%s,%s,%s,%s,%s));", nbProg, date.get(0), date.get(1),
				date.get(2), date.get(3), date.get(4));
	}

	/**
	 * Write the starting of a scenario
	 * @param idScenar The identifier of the scenario
	 */
	public void startScenario(String idScenar) {
		currentScenario = idScenar;
		scenario = String.format("\n/* Scenario %s */\nString %s_contenu = \"", idScenar, idScenar);
		habitat = String.format("case %d : // Scenario %s\n", this.numScenario(idScenar), idScenar);
	}

	/**
	 * Write the ending of the scenario
	 */
	public void endScenario() {
		scenario += String.format("\"; CScenario %s = new CScenario(\"%s\", %s_contenu);", currentScenario,
				currentScenario, currentScenario);
		scenario += String.format("ma_liste_scenarios.add(%s);", currentScenario);
		habitat += " break;";
		scenarios += scenario;
		habitats  += habitat;
	}

	/**
	 * Write the starting of a foreach
	 * @param type The device type use by the foreach
	 */
	public void startForeach(TypeAppareil type) {
		String foreach = String.format(
				"for(CAppareil appareil : this.l_appareils) { if(appareil.typeAppareil.equals(TypeAppareil.%s)) { ",
				TypeAppareilToStringTransformer.stringValue(type));
		scenario += foreach;
		habitat += foreach;
		inFor = true;
	}

	/**
	 * Write the ending of a foreach
	 */
	public void endForeach() {
		scenario += "}}";
		habitat += "}}";
		inFor = false;
	}

	/**
	 * Write the starting of a message
	 */
	public void startMessage() {
		scenario += "System.out.println(\\\"\\\"";
		habitat += "System.out.println(\"\"";
	}

	/**
	 * Write the ending of a message
	 */
	public void endMessage() {
		scenario += ");";
		habitat += ");";
	}

	/**
	 * Add a message to print
	 * @param chaine The string to print, it already contains quotes but we need to escape them
	 */
	public void addMessageChaine(String chaine) {
		scenario += "+\\\"" + chaine.substring(1, chaine.length()-2) + "\\\"";
		habitat += "+" + chaine;
	}

	/**
	 * Add a device identifier string to print in a message
	 * @param id The identifier string
	 */
	public void addMessageAppareilIdentifier(String id) {
		scenario += "+\\\"" + id + "\\\"";
		habitat += "+ appareil.getNomAppareil()";
	}

	/**
	 * Write a simple instruction like {@code device.method}
	 * @param id The identifier
	 * @param method The method to call
	 */
	public void addAction(String id, String method) {
		if(!inFor && !inForIf){
			scenario += String.format("for(CAppareil appareil : this.l_appareils) { if(appareil.getNomAppareil().equals(\\\"%s\\\")) { ",id);
			habitat += String.format("for(CAppareil appareil : this.l_appareils) { if(appareil.getNomAppareil().equals(\"%s\")) { ",id);
		}
		String str = String.format("appareil.appliquer(TypeActionAppareil.%s);", method.toUpperCase());
		if(!inFor && !inForIf)
			str += "}}";
		scenario += str;
		habitat += str;
	}
	
	/**
	 * Add an execution of a scenario
	 * @param idScenar The ID of the scenario to execute
	 */
	public void addExecScenario(String idScenar) {
		String str = "this.execScenarioNum("+this.numScenario(idScenar)+");";
		scenario += str;
		habitat += str;
	}
	
	/**
	 * Get the number of a scenario. This method not handle an unknown scenario.
	 * The method must be used carefully in a specific context.
	 * 
	 * @param idScenar The scenario identifier
	 * @return The number of a scenario
	 */
	private int numScenario(String idScenar) {
		int i=0;
		Iterator<IdentifierScenario> it = identifiers.getScenario().iterator();
		while(it.hasNext()) {
			IdentifierScenario id = it.next();
			if(id.id.equals(idScenar))
				break;
			i++;
		}
		return i;
	}

	/**
	 * Start a if with a state condition
	 * @param id The identifier of the device
	 * @param state The state in which the device should be
	 */
	public void startIfWithCond(String id, String state) {
		
		if(!inFor){
			scenario += String.format("for(CAppareil appareil : this.l_appareils) { if(appareil.getNomAppareil().equals(\\\"%s\\\")) { ",id);
			habitat += String.format("for(CAppareil appareil : this.l_appareils) { if(appareil.getNomAppareil().equals(\"%s\")) { ",id);
			inForIf = true;
		}
		
		String str = String.format("if(appareil.etatAppareil.equals(TypeEtatAppareil.%s)) {", state.toUpperCase());
		
		scenario += str;
		habitat += str;
	}
	
	/**
	 * Ending of the if
	 */
	public void endIf() {
		String str = "";
		if(inForIf){
			str += "}}";
			inForIf = false;
		}
		str += "}";
		
		scenario += str;
		habitat += str;
	}
	
	/**
	 * Add an else clause to a if/else
	 */
	public void addElse() {
		scenario += "} else {";
		habitat += "} else {";
	}
}
