import java.util.ArrayList;
import java.util.HashMap;

/**
 * An identifier for devices
 * 
 * @author Jimmy Tournemaine
 * @author Pierre Siguret
 */
public class IdentifierAppareil extends Identifier {
	
	public static HashMap<TypeAppareil,ArrayList<String>> methods;
	public static HashMap<TypeAppareil,ArrayList<String>> properties;
	TypeAppareil type;
	
	/**
	 * Create an identifier defined by an id and the kind of device
	 * @param id The identifier string
	 * @param type The type of device
	 */
	public IdentifierAppareil(String id, TypeAppareil type) {
		super(id);
		this.type = type;
	}
	
	static {
	prepareMethods();
	prepareProperties();
	}
	
	/**
	 * Prepare methods that a device of a type is allow to call
	 */
	private static void prepareMethods() {
		methods = new HashMap<TypeAppareil,ArrayList<String>>();

		/* Eclairage */
		ArrayList<String> list = new ArrayList<String>();
		list.add("allumer"); list.add("tamiser"); list.add("eteindre");
		methods.put(TypeAppareil.ECLAIRAGE, list);
		
		/* Alarme */
		list = new ArrayList<String>();
		list.add("allumer"); list.add("allumer_partiel"); list.add("eteindre");
		methods.put(TypeAppareil.ALARME, list);

		/* Chauffage */
		list = new ArrayList<String>();
		list.add("allumer"); list.add("allumer_eco"); list.add("eteindre");
		methods.put(TypeAppareil.CHAUFFAGE, list);
		
		/* FENETRE */
		list = new ArrayList<String>();
		list.add("ouvrir"); list.add("ouvrir_partiel"); list.add("fermer"); list.add("fermer_partiel");
		methods.put(TypeAppareil.FENETRE, list);

		/* Volet */
		list = new ArrayList<String>();
		list.add("ouvrir"); list.add("ouvrir_partiel"); list.add("fermer"); list.add("fermer_partiel");
		methods.put(TypeAppareil.VOLET, list);
		
		/* Autre */
		list = new ArrayList<String>();
		list.add("allumer"); list.add("eteindre");
		methods.put(TypeAppareil.OTHER, list);
		methods.put(TypeAppareil.OTHER_CAFETIERE, list);
		methods.put(TypeAppareil.OTHER_HIFI, list);
		methods.put(TypeAppareil.OTHER_VID_PRO, list);
		methods.put(TypeAppareil.OTHER_LAV_LIN, list);
		methods.put(TypeAppareil.OTHER_SEC_LIN, list);
		methods.put(TypeAppareil.OTHER_ORDINATEUR, list);
		methods.put(TypeAppareil.OTHER_PORTAIL, list);
		methods.put(TypeAppareil.OTHER_TV, list);
		methods.put(TypeAppareil.OTHER_LAV_VAI, list);
	}
	
	/**
	 * Prepare states that a device of a type can have
	 * @author Pierre Siguret
	 */
	private static void prepareProperties() {
		properties = new HashMap<TypeAppareil,ArrayList<String>>();

		/* Eclairage */
		ArrayList<String> list = new ArrayList<String>();
		list.add("allume"); list.add("demi"); list.add("eteint");
		properties.put(TypeAppareil.ECLAIRAGE, list);
		
		/* Alarme */
		list = new ArrayList<String>();
		list.add("allume"); list.add("demi"); list.add("eteint");
		properties.put(TypeAppareil.ALARME, list);

		/* Chauffage */
		list = new ArrayList<String>();
		list.add("allume"); list.add("eteint"); list.add("eco");
		properties.put(TypeAppareil.CHAUFFAGE, list);
		
		/* FENETRE */
		list = new ArrayList<String>();
		list.add("ouvert"); list.add("ferme"); list.add("demi");
		properties.put(TypeAppareil.FENETRE, list);

		/* Volet */
		list = new ArrayList<String>();
		list.add("ouvert"); list.add("ferme"); list.add("demi");
		properties.put(TypeAppareil.VOLET, list);
		
		/* Autre */
		list = new ArrayList<String>();
		list.add("allume"); list.add("eteint");
		properties.put(TypeAppareil.OTHER_CAFETIERE, list);
		properties.put(TypeAppareil.OTHER_HIFI, list);
		properties.put(TypeAppareil.OTHER_VID_PRO, list);
		properties.put(TypeAppareil.OTHER_LAV_LIN, list);
		properties.put(TypeAppareil.OTHER_SEC_LIN, list);
		properties.put(TypeAppareil.OTHER_ORDINATEUR, list);
		properties.put(TypeAppareil.OTHER_PORTAIL, list);
		properties.put(TypeAppareil.OTHER_TV, list);
		properties.put(TypeAppareil.OTHER_LAV_VAI, list);
	}

}
