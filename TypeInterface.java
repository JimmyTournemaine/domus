/**
 * Enum types of interfaces
 * @author Jimmy Tournemaine
 * @author Pierre Siguret
 */
public enum TypeInterface {
	MOBILE,
	TELEPHONE,
	TABLETTE,
	INTERRUPTEUR,
	TELECOMMANDE
}
