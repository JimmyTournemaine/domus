JCC = javac
JFLAGS = -g
JFLEX := $(shell command -v jflex 2> /dev/null)
JCUP := $(shell command -v java_cup 2> /dev/null)
CONFLICTS :=  
default: exec

lexer: base_lex.lex
ifndef JFLEX
	java JFlex.Main base_lex.lex
else
	jflex base_lex.lex
endif

parser: parser.cup
ifndef JCUP
	java java_cup.Main ${CONFLICTS} parser.cup
else
	java_cup ${CONFLICTS} *.cup
endif

exec: lexer parser
	${JCC} ${JFLAGS} parser.java sym.java Yylex.java

clean: 
	rm -f *.class parser.java sym.java  Yylex.java *.class~ *.java~ CMaisonUser.java HabitatSpecific.java
