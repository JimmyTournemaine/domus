/**
 * An identifier for an interface
 * @author Jimmy Tournemaine
 * @author Pierre Siguret
 */
public class IdentifierInterface extends Identifier {

	TypeInterface type;
	
	public IdentifierInterface(String id, TypeInterface type) {
		super(id);
		this.type = type;
	}

}
