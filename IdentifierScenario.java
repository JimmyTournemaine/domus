/**
 * An identifier of a scenario
 * 
 * @author Jimmy Tournemaine
 * @author Pierre Siguret
 */
public class IdentifierScenario extends Identifier {

	public IdentifierScenario(String id) {
		super(id);
	}

}
