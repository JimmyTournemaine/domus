/**
 * Types of devices
 * 
 * @author Pierre Siguret
 * @author Jimmy Tournemaine
 */
public enum TypeAppareil {
	ECLAIRAGE, 
	ALARME, 
	CHAUFFAGE, 
	FENETRE, 
	VOLET, 
	OTHER, 
	OTHER_CAFETIERE, 
	OTHER_HIFI, 
	OTHER_VID_PRO, 
	OTHER_LAV_LIN, 
	OTHER_SEC_LIN,
	OTHER_ORDINATEUR, 
	OTHER_PORTAIL, 
	OTHER_TV, 
	OTHER_LAV_VAI
};
