import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

/**
 * An identifier for a set of devices
 * 
 * @author Jimmy Tournemaine
 * @author Pierre Siguret
 */
public class IdentifierSet extends Identifier {

	public Set<Identifier> elements;
	
	/**
	 * Create a set of identifiers
	 * @param id The identifier of the set
	 * @param ids The identifiers in the set
	 */
	public IdentifierSet(String id, ArrayList<Identifier> ids) {
		super(id);
		this.elements = new HashSet<Identifier>(ids);
	}

}
