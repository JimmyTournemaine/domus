/**
 * A basic identifier
 * @author Jimmy Tournemaine
 * @author Pierre Siguret
 */
public class Identifier {

	protected String id;
	
	public Identifier(String id) {
		this.id = id;
	}
	
	public String toString() {
		return id;
	}

	public boolean hasId(String id2) {
		return id2.equals(id);
	}
	
}
