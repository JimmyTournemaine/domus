import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

/**
 * The object is use to handle the semantics analysis
 * @author Jimmy Tournemaine
 * @author Pierre Siguret
 */
public class Identifiers {

	ArrayList<Identifier> ids;
	private static ArrayList<String> keywords = new ArrayList<String>();

	static {
		keywords.add("PROGRAMME_DOMUS");
		keywords.add("DECLARATION_APPAREILS");
		keywords.add("DECLARATION_INTERFACES");
		keywords.add("DECLARATION_SCENARII");
		keywords.add("DECLARATION_COMMANDES");
		keywords.add("SCENARIO");
		keywords.add("definir");
		keywords.add("executer_scenario");
		keywords.add("associer");
		keywords.add("programmer");
		keywords.add("message");
		keywords.add("pourtout");
		keywords.add("faire");
		keywords.add("fait");
		keywords.add("si");
		keywords.add("alors");
		keywords.add("sinon");
		keywords.add("fsi");
		keywords.add("ouvrir");
		keywords.add("fermer");
		keywords.add("eteindre");
		keywords.add("allumer");
		keywords.add("tamiser");
		keywords.add("etat");
		keywords.add("allumer_partiel");
		keywords.add("allumer_eco");
		keywords.add("ourir_partiel");
		keywords.add("fermer_partiel");
		keywords.add("allume");
		keywords.add("eteint");
		keywords.add("demi");
		keywords.add("eco");
		keywords.add("ouvert");
		keywords.add("ferme");
		keywords.add("eclairage");
		keywords.add("volet");
		keywords.add("chauffage");
		keywords.add("alarme");
		keywords.add("fenetre");
		keywords.add("autre_appareil");
		keywords.add("interrupteur");
		keywords.add("mobile");
		keywords.add("telephone");
		keywords.add("telecommande");
		keywords.add("tv");
		keywords.add("hifi");
		keywords.add("cafetiere");
		keywords.add("video_proj");
		keywords.add("lave_vaisselle");
		keywords.add("lave_linge");
		keywords.add("seche_linge");
		keywords.add("ordinateur");
		keywords.add("portail");
	}

	/**
	 * Create an empty list of known identifiers
	 */
	public Identifiers() {
		ids = new ArrayList<Identifier>();
	}

	/**
	 * Check if an identifier is known
	 * @param id The identifier as string
	 * @return A boolean that represents if the identifier exists or not
	 */
	public boolean exists(String id) {
		for (Identifier i : ids) {
			if (id.equals(i.id)) {
				return true;
			}
		}
		return false;
	}

	/**
	 * Add an identifier to the list of known identifier
	 * @param id The identifier to add
	 */
	public void addIdentifier(Identifier id) {
		ids.add(id);
	}

	/**
	 * Check if a device identifier (as string) can call a method depending of its type
	 * @param id The identifier (as string)
	 * @param method The method (as string) to call on the identifier
	 * @return A boolean that represents if the identifier can call the method
	 */
	public boolean exec(String id, String method) {
		Identifier identifier = this.findById(id);
		if (identifier == null)
			return false;
		if (!(identifier instanceof IdentifierAppareil))
			return false;

		IdentifierAppareil ia = (IdentifierAppareil) identifier;
		for (String m : IdentifierAppareil.methods.get(ia.type)) {
			if (m.equals(method))
				return true;
		}

		return false;
	}

	/**
	 * Check if the state of the identifier can be {@code property}
	 * @param id The identifier
	 * @param property The value of the state
	 * @return A boolean that represents if the identifier can be {@code property}
	 */
	public boolean has(String id, String property) {
		Identifier identifier = this.findById(id);
		if (identifier == null)
			return false;
		if (!(identifier instanceof IdentifierAppareil))
			return false;

		IdentifierAppareil ia = (IdentifierAppareil) identifier;
		for (String m : IdentifierAppareil.properties.get(ia.type)) {
			if (m.equals(property))
				return true;
		}

		return false;
	}

	/**
	 * Try to find an identifier by its String value
	 * @param id The identifier string
	 * @return Identifier|null The matching identifier or null if anybody has been found
	 */
	private Identifier findById(String id) {
		Iterator<Identifier> it = ids.iterator();
		while (it.hasNext()) {
			Identifier i = it.next();
			if (i.hasId(id))
				return i;
		}
		return null;
	}

	/**
	 * Check if an identifier is an identifier for an interface
	 * @param id The identifier string
	 * @return If the identifier is an interface
	 */
	public boolean isInterface(String id) {
		Identifier identifier = this.findById(id);
		if (identifier == null)
			return false;
		return identifier instanceof IdentifierInterface;
	}

	/**
	 * Check if the identifier is an identifier for a device
	 * @param id The identifier string
	 * @return If the identifier is a device
	 */
	public boolean isAppareil(String id) {
		Identifier identifier = this.findById(id);
		if (identifier == null)
			return false;
		return identifier instanceof IdentifierAppareil;
	}

	/**
	 * Check if the identifier is a scenario
	 * @param id The identifier string
	 * @return If the identifier is a scenario
	 */
	public boolean isScenario(String id) {
		Identifier identifier = this.findById(id);
		if (identifier == null)
			return false;
		return identifier instanceof IdentifierScenario;
	}

	/**
	 * Check if a date format is valid
	 * @param date A list of integers that represents a date [year,month,day,hours,minutes]
	 * @throws Exception Throw an exception if the date is not valid
	 */
	public void isDateValid(LinkedList<Integer> date) throws Exception {
		if (date.get(1) != -1) {
			if (date.get(1) > 12 || date.get(1) < 1)
				throw new Exception(String.format("Date format error : %d is an invalid month value", date.get(1)));
		}
		if (date.get(0) != -1) {
			switch (date.get(1)) {
			case 1:
			case 3:
			case 5:
			case 7:
			case 8:
			case 10:
			case 12:
				if (date.get(2) < 1 || date.get(2) > 31)
					throw new Exception(String.format("Date format error : %d is an invalid day value", date.get(2)));
				break;
			case 2:
				int maxD = (date.get(1) % 4 == 0 && date.get(1) % 100 != 0 || date.get(0) % 400 == 0) ? 29 : 28;
				if (date.get(2) < 1 || date.get(2) > maxD)
					throw new Exception(String.format("Date format error : %d is an invalid %d-days-month value", date.get(2), maxD));
				break;
			case 4:
			case 6:
			case 9:
			case 11:
				if (date.get(2) < 1 || date.get(2) > 30)
					throw new Exception(String.format("Date format error : %d is an invalid 30-days-month value", date.get(2)));
			}
		}
		if (date.get(3) != -1) {
			if (date.get(3) < 0 || date.get(3) > 23)
				throw new Exception(String.format("Date format error : %d is an invalid hour value", date.get(3)));
		}
		if (date.get(4) != -1) {
			if (date.get(4) < 0 || date.get(4) > 59)
				throw new Exception(String.format("Date format error : %d is an invalid minute value", date.get(4)));
		}
	}

	/**
	 * Get a list of device identifiers
	 * @return The list of known device identifiers
	 */
	public List<IdentifierAppareil> getAppareils() {
		ArrayList<IdentifierAppareil> appareils = new ArrayList<IdentifierAppareil>();
		for (Identifier id : ids) {
			if (id instanceof IdentifierAppareil) {
				appareils.add((IdentifierAppareil) id);
			}
		}
		return appareils;
	}

	/**
	 * Get the sets of debices identifiers
	 * @return The list of known devices identifiers
	 */
	public List<IdentifierSet> getSets() {
		ArrayList<IdentifierSet> appareils = new ArrayList<IdentifierSet>();
		for (Identifier id : ids) {
			if (id instanceof IdentifierSet) {
				appareils.add((IdentifierSet) id);
			}
		}
		return appareils;
	}

	/**
	 * Create a set of identifiers
	 * @param id The identifier of the set to create
	 * @param elementsId The identifiers (as string) to add to the set
	 * @throws Exception If you try to add an unknown identifier to the new set
	 */
	public void createSet(String id, ArrayList<String> elementsId) throws Exception {
		ArrayList<Identifier> list = new ArrayList<Identifier>();
		for (String eId : elementsId) {
			Identifier i = this.findById(eId);
			if (i == null) {
				throw new Exception(eId + " unknown. You need to declare it first.");
			}
			list.add(i);
		}
		ids.add(new IdentifierSet(id, list));
	}

	/**
	 * Remove an identifier
	 * @param id The identifier string that you remove
	 * @return
	 */
	public boolean removeId(String id) {
		Identifier i = this.findById(id);
		if (i == null)
			return false;
		return ids.remove(i);
	}

	/**
	 * Get the interface identifiers
	 * @return The list of iterface identifiers
	 */
	public List<IdentifierInterface> getInterfaces() {
		ArrayList<IdentifierInterface> theInterface = new ArrayList<IdentifierInterface>();
		for (Identifier id : ids) {
			if (id instanceof IdentifierInterface) {
				theInterface.add((IdentifierInterface) id);
			}
		}
		return theInterface;
	}

	/**
	 * Get scenario identifiers
	 * @return The list of known scenario identifiers
	 */
	public List<IdentifierScenario> getScenario() {
		ArrayList<IdentifierScenario> theScenar = new ArrayList<IdentifierScenario>();
		for (Identifier id : ids) {
			if (id instanceof IdentifierScenario) {
				theScenar.add((IdentifierScenario) id);
			}
		}
		return theScenar;
	}

	/**
	 * Check if a string is a reserved keyword
	 * @param id The identifier to check
	 * @return If the given string is a reserved keyword
	 */
	public boolean isKeyWord(String id) {
		return keywords.contains(id);
	}
}