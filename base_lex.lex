// Specification JFlex
import java_cup.runtime.Symbol;

%%
%unicode
%cup
%line
%column

%{
	public int getYyLine(){
		return yyline+1;
	}
	public int getYyColumn(){
		return yycolumn+1;
	}
	public String getYyText(){
		return yytext();
	}
%}


chiffre       = [0-9]
entier        = {chiffre}+
ws 	      = (\n|\r|\t|\f|\b|\ )*
chaine        = \"[^\"]*\"
ident 	      = _?[a-z][A-Za-z0-9_]*
erreur_ident  = [0-9]+[a-zA-Z]+[A-Za-z0-9_]*
erreur_chaine = \"[^\"\n]*\n
commentaire   = \/\/.*~\n
egal	      = \=
comparaison   = \=\=
comma	      = ,
el	      = \.

deb                 = <PROGRAMME_DOMUS>
fin                 = <\/PROGRAMME_DOMUS>
decl_appareils_deb  = <DECLARATION_APPAREILS>
decl_appareils_fin  = <\/DECLARATION_APPAREILS>
decl_interfaces_deb = <DECLARATION_INTERFACES>
decl_interfaces_fin = <\/DECLARATION_INTERFACES>
decl_scenarii_deb   = <DECLARATION_SCENARII>
decl_scenarii_fin   = <\/DECLARATION_SCENARII>
scenario_deb        = <SCENARIO\ 
scenario_fin        = <\/SCENARIO\ 
decl_commandes_deb  = <DECLARATION_COMMANDES>
decl_commandes_fin  = <\/DECLARATION_COMMANDES>
%%

definir {return new Symbol(sym.DEFINIR);}
executer_scenario {return new Symbol(sym.EXEC_SCEN);}
associer {return new Symbol(sym.ASSOCIER);}
programmer {return new Symbol(sym.PROGRAMMER);}
message {return new Symbol(sym.MESSAGE);}
pourtout {return new Symbol(sym.POURTOUT);}
faire {return new Symbol(sym.FAIRE);}
fait {return new Symbol(sym.FAIT);}
si {return new Symbol(sym.SI);}
alors {return new Symbol(sym.ALORS);}
sinon {return new Symbol(sym.SINON);}
fsi {return new Symbol(sym.FSI);}
ouvrir {return new Symbol(sym.OUVRIR);}
fermer {return new Symbol(sym.FERMER);}
eteindre {return new Symbol(sym.ETEINDRE);}
allumer {return new Symbol(sym.ALLUMER);}
tamiser {return new Symbol(sym.TAMISER);}
etat {return new Symbol(sym.ETAT);}
allumer_partiel {return new Symbol(sym.ALL_PAR);}
allumer_eco {return new Symbol(sym.ALL_ECO);}
ouvrir_partiel {return new Symbol(sym.OUV_PAR);}
fermer_partiel {return new Symbol(sym.FER_PAR);}
allume {return new Symbol(sym.ALLUME);}
eteint {return new Symbol(sym.ETEINT);}
demi {return new Symbol(sym.DEMI);}
eco {return new Symbol(sym.ECO);}
ouvert {return new Symbol(sym.OUVERT);}
ferme {return new Symbol(sym.FERME);}
eclairage {return new Symbol(sym.ECLAIRAGE);}
volet {return new Symbol(sym.VOLET);}
chauffage {return new Symbol(sym.CHAUFFAGE);}
alarme {return new Symbol(sym.ALARME);}
fenetre {return new Symbol(sym.FENETRE);}
autre_appareil {return new Symbol(sym.AUT_APP);}
interrupteur {return new Symbol(sym.INTERRUPTEUR);}
mobile {return new Symbol(sym.MOBILE);}
telephone {return new Symbol(sym.TELEPHONE);}
telecommande {return new Symbol(sym.TELECOMMANDE);}
tablette {return new Symbol(sym.TABLETTE);}
tv {return new Symbol(sym.TV);}
hifi {return new Symbol(sym.HIFI);}
cafetiere {return new Symbol(sym.CAFETIERE);}
video_proj {return new Symbol(sym.VID_PRO);}
lave_vaisselle {return new Symbol(sym.LAV_VAI);}
lave_linge {return new Symbol(sym.LAV_LIN);}
seche_linge {return new Symbol(sym.SEC_LIN);}
ordinateur {return new Symbol(sym.ORDINATEUR);}
portail {return new Symbol(sym.PORTAIL);}
{comparaison} { return new Symbol(sym.COMPARAISON);}
{egal} { return new Symbol(sym.EGAL);}
{deb} { return new Symbol(sym.DEB);}
{fin} {return new Symbol(sym.FIN);}
{decl_appareils_deb} {return new Symbol(sym.APPS_DEB);}
{decl_appareils_fin} {return new Symbol(sym.APPS_FIN);}
{decl_interfaces_deb} {return new Symbol(sym.INTER_DEB);}
{decl_interfaces_fin} {return new Symbol(sym.INTER_FIN);}
{decl_scenarii_deb} {return new Symbol(sym.SCENARII_DEB);}
{decl_scenarii_fin} {return new Symbol(sym.SCENARII_FIN);}
{scenario_deb} {return new Symbol(sym.SCENAR_DEB);}
{scenario_fin} {return new Symbol(sym.SCENAR_FIN);}
{decl_commandes_deb} {return new Symbol(sym.COM_DEB);}
{decl_commandes_fin} {return new Symbol(sym.COM_FIN);}
{chaine} {return new Symbol(sym.CHAINE, yytext()); }
{ident} { return new Symbol(sym.IDENT, yytext()); }
{erreur_ident} 	{System.err.println("[lex] Lexical error at line "+(yyline+1)+" column "+(yycolumn+1)+" : "+yytext()+" : invalid identifier syntax"); }
{entier} { return new Symbol(sym.ENTIER, new Integer(yytext())); }
{commentaire} {}
{comma} {return new Symbol(sym.COMMA); }
{el} {return new Symbol(sym.EL); }
"(" { return new Symbol(sym.LPAR); }
")" { return new Symbol(sym.RPAR); } 
">" { return new Symbol(sym.RCHEV); }
";" { return new Symbol(sym.SEMI); }
":" { return new Symbol(sym.COLON); }
"{" {return new Symbol(sym.LACC);}
"}" {return new Symbol(sym.RACC);}
"_" {return new Symbol(sym.UNDER);}
{ws} {}

{erreur_chaine} {System.err.println("[lex] Lexical error at line "+(yyline+1)+" column "+(yycolumn+1)+" : "+yytext().replace("\n", "")+" : end of string expected"); }
.  		{System.err.println("[lex] Lexical error at line "+(yyline+1)+" column "+(yycolumn+1)+" : "+yytext()+" : unknown character"); } 


